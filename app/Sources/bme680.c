/*
* bme680.c
*/
#include "cmsis_os.h"
#include "i2c.h"
#include "bme680.h"
#include "math.h"

/***** PRIVATE FUNCTION PROTPTYPE *****/
int8_t Conv_bin2hex(uint8_t bins);

/***** PUBLIC FUNCTION PROTOTYPE *****/

/***** STATIC VARIABLE *****/
static float const_array1[16] = {1, 1, 1, 1, 1, 0.99, 1, 0.992, 1, 1, 0.998, 0.995, 1, 0.99, 1, 1};
static double const_array2[16] = {8000000.0, 4000000.0, 2000000.0, 1000000.0, 499500.4995, 248262.1648, 125000.0, 
63004.03226, 31281.28128, 15625.0, 7812.5, 3906.25, 1953.125, 976.5625, 488.28125, 244.140625};

/***** FUNCTIONS *****/
int8_t Conv_bin2hex(uint8_t bins){
  uint8_t hd[] = "0123456789ABCDEF" ;
  return(hd[bins]);
}

uint8_t BME680_init(void){ 
    devparam.communication = 0;
    devparam.I2CAddress = 0x76;
    devparam.sensorMode = 0x01; // default sensor-mode
    devparam.IIRfilter = 0x04;
    devparam.tempOversampling = 0x05;
    devparam.pressOversampling = 0x05;
    devparam.humidOversampling = 0x05;
    devparam.pressureSeaLevel = 1013.25;
    devparam.tempOutsideCelsius = 15;
    devparam.tempOutsideFahrenheit = 59;
    devparam.target_temp = 320;
    devparam.amb_temp;
    devparam.hotplate_profile;

  
  // softreset
  BME680_reset();
  
  // set Oversampling
  //BME680_setOversampling();
  
  //
  I2C0WriteTransaction( BME680_I2C_ADDR, BME680_CTRL_GAS_1, 0 );
  
  // set IIR Filter confficient
  BME680_setFillter();
  
  // read Coefficients
  BME680_readCoefficients();
  
  BME680_calculateHotPlateTime();
  BME680_calculateHotPlateRes();
  BME680_setHotPlateProfile();
  
  return BME680_checkID();
}
void BME680_reset(void){
  I2C0WriteTransaction( BME680_I2C_ADDR, BME680_RESET, 0xB6 );
}
void BME680_setOversampling(void){
  uint8_t setvalue = 0;
  
  // set Humidity oversampling 
  setvalue = devparam.humidOversampling & (7);
  I2C0WriteTransaction( BME680_I2C_ADDR, BME680_CTRL_HUM, setvalue );

  // set Temperature oversampling
  // set Pressure oversampling
  setvalue = ( devparam.tempOversampling<<5) & 0xE0;   // temp
  setvalue |= (devparam.pressOversampling<<2) & 0x1C;  // pressure
  I2C0WriteTransaction( BME680_I2C_ADDR, BME680_CTRL_MEAS, setvalue );  
}
void BME680_setHeater(void){
  /*** Set Heater profile ***/
  /*** reg=0x71 set to 0x00 ***/
  I2C0WriteTransaction( BME680_I2C_ADDR, BME680_CTRL_GAS_1, 0 );
}
void BME680_setFillter(void){
  uint8_t setvalue = 0;
  /*** FILTER SETTING ***/
  //0b000:      factor 0 (filter off)
  //0b001:      factor 1
  //0b010:      factor 3
  //0b011:      factor 7
  //0b100:      factor 15 (default value)
  //0b101:      factor 31
  //0b110:      factor 63
  //0b111:      factor 127 (maximum value)
  
  // set factor 15
  setvalue = (devparam.IIRfilter<<2) & 0x1C;
  I2C0WriteTransaction( BME680_I2C_ADDR, BME680_CONFIG, setvalue );
}
void BME680_readCoefficients(void){
	bme680_coefficients.dig_T1 = ((uint16_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_T1_MSB) << 8) + I2C0ReadTransaction( BME680_I2C_ADDR, BME680_DIG_T1_LSB));
	bme680_coefficients.dig_T2 = ((int16_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_T2_MSB) << 8) + I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_T2_LSB));
	bme680_coefficients.dig_T3 = ((int8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_T3)));
	
	bme680_coefficients.dig_P1 = ((uint16_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P1_MSB) << 8) + I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P1_LSB));
	bme680_coefficients.dig_P2 = ((int16_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P2_MSB) << 8) + I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P2_LSB));
	bme680_coefficients.dig_P3 = ((int8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P3)));
	bme680_coefficients.dig_P4 = ((int16_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P4_MSB) << 8) + I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P4_LSB));
	bme680_coefficients.dig_P5 = ((int16_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P5_MSB) << 8) + I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P5_LSB));
	bme680_coefficients.dig_P6 = ((int8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P6)));
	bme680_coefficients.dig_P7 = ((int8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P7)));
	bme680_coefficients.dig_P8 = ((int16_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P8_MSB) << 8) + I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P8_LSB));
	bme680_coefficients.dig_P9 = ((int16_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P9_MSB) << 8) + I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P9_LSB));
	bme680_coefficients.dig_P10 = ((uint8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_P10)));
	
	bme680_coefficients.dig_H1 = (uint16_t)(((uint16_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_H1_MSB)) << 4) + ((uint8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_H1_LSB)) & BME680_BIT_MASK_H1_DATA));
	bme680_coefficients.dig_H2 = (uint16_t)(((uint16_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_H2_MSB)) << 4) + (((uint8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_H2_LSB))) >> 4));
	bme680_coefficients.dig_H3 = (int8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_H3));
	bme680_coefficients.dig_H4 = (int8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_H4));
	bme680_coefficients.dig_H5 = (int8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_H5));
	bme680_coefficients.dig_H6 = (uint8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_H6));
	bme680_coefficients.dig_H7 = (int8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_H7));

//The following calibration parameters are used to convert the target temperature of the hot plate to a register value
//This register value is written to the registers res_heat_x<7:0> (see Datasheet, "Gas sensor heating and measurement", pages 18 and 19) 
	
	bme680_coefficients.par_g1 = (int8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_G1));	
	bme680_coefficients.par_g2 = (int16_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_G2_MSB) << 8) + I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_G2_LSB);	
	bme680_coefficients.par_g3 = (int8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_DIG_G3));
	
	bme680_coefficients.res_heat_range = (int8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_RES_HEAT_RG));
	bme680_coefficients.res_heat_val   = (int8_t)(I2C0ReadTransaction( BME680_I2C_ADDR,BME680_RES_HEAT_VL));
}
void BME680_calculateHotPlateTime(void){
  uint8_t setvalue=0x65;  // 148ms heating duration
  I2C0WriteTransaction( BME680_I2C_ADDR, BME680_GAS_WAIT_0, setvalue );
}
void BME680_calculateHotPlateRes(void){
	double var1;
	double var2;
	double var3;
	double var4;
	double var5;
	
	
	var1 = ((double)bme680_coefficients.par_g1 / 16.0) + 49.0;
	var2 = (((double)bme680_coefficients.par_g2 / 32768.0) * 0.0005) + 0.00235;
	var3 = (double)bme680_coefficients.par_g3 / 1024.0;
	var4 = var1 * (1.0 + (var2 * (double)devparam.target_temp));
	var5 = var4 + (var3 * (double)BME680_readTempC());
	
	bme680_coefficients.res_heat_0 = (uint8_t)(3.4 * ((var5 * (4.0 / (4.0 + (double)bme680_coefficients.res_heat_range)) * (1.0 / (1.0 + ((double)bme680_coefficients.res_heat_val * 0.002)))) - 25));
	
	I2C0WriteTransaction( BME680_I2C_ADDR, BME680_RES_HEAT_0, (uint8_t)bme680_coefficients.res_heat_0);
}
void BME680_setHotPlateProfile(void){
  	uint8_t value=0;
	value = (((uint8_t)devparam.hotplate_profile) & 0x0F) | 0x10;
	I2C0WriteTransaction( BME680_I2C_ADDR, BME680_CTRL_GAS_1, value);
}
uint8_t BME680_checkID(void){
  	uint8_t chipID;
	chipID = I2C0ReadTransaction( BME680_I2C_ADDR, BME680_CHIP_ID);
	return chipID;
}

float BME680_readTempC(void)
{	
    if (devparam.tempOversampling == 0x00)        //disabling the temperature measurement function
    {
            return 0;
    }
          
    else
    {
            int32_t adc_T;
            adc_T = (uint32_t)I2C0ReadTransaction( BME680_I2C_ADDR,BME680_TEMPERATURE_MSB) << 12;
            adc_T |= (uint32_t)I2C0ReadTransaction( BME680_I2C_ADDR,BME680_TEMPERATURE_LSB) << 4;
            adc_T |= (I2C0ReadTransaction( BME680_I2C_ADDR,BME680_TEMPERATURE_XLSB) >> 4) & 0x0F;
            
            int64_t var1, var2;
            
            var1 = ((((adc_T>>3) - ((int32_t)bme680_coefficients.dig_T1<<1))) * ((int32_t)bme680_coefficients.dig_T2)) >> 11;
            var2 = (((((adc_T>>4) - ((int32_t)bme680_coefficients.dig_T1)) * ((adc_T>>4) - ((int32_t)bme680_coefficients.dig_T1))) >> 12) *
            ((int32_t)bme680_coefficients.dig_T3)) >> 14;
            t_fine = var1 + var2;
            float T = (t_fine * 5 + 128) >> 8;
            T = T / 100;
            return T;
    }
}

float BME680_readTempF(void)
{	
    if (devparam.tempOversampling == 0x00)	//disabling the temperature measurement function
    {
            return 0;
    }
    
    else
    {
            int32_t adc_T;
            adc_T = (uint32_t)I2C0ReadTransaction( BME680_I2C_ADDR,BME680_TEMPERATURE_MSB) << 12;
            adc_T |= (uint32_t)I2C0ReadTransaction( BME680_I2C_ADDR,BME680_TEMPERATURE_LSB) << 4;
            adc_T |= (I2C0ReadTransaction( BME680_I2C_ADDR,BME680_TEMPERATURE_XLSB) >> 4) & 0x0F;
            
            int64_t var1, var2;
            
            var1 = ((((adc_T>>3) - ((int32_t)bme680_coefficients.dig_T1<<1))) * ((int32_t)bme680_coefficients.dig_T2)) >> 11;
            var2 = (((((adc_T>>4) - ((int32_t)bme680_coefficients.dig_T1)) * ((adc_T>>4) - ((int32_t)bme680_coefficients.dig_T1))) >> 12) *
            ((int32_t)bme680_coefficients.dig_T3)) >> 14;
            t_fine = var1 + var2;
            float T = (t_fine * 5 + 128) >> 8;
            T = T / 100;
            T = (T * 1.8) + 32;
            return T;
    }
}
float BME680_convertTempKelvin(void)
{
	float tempOutsideKelvin;	
	
	if (devparam.tempOutsideCelsius != 999 & devparam.tempOutsideFahrenheit == 999 )   
	{
		tempOutsideKelvin = devparam.tempOutsideCelsius;
		tempOutsideKelvin = tempOutsideKelvin + 273.15;
		return tempOutsideKelvin;		
	}
	
	if (devparam.tempOutsideCelsius != 999 & devparam.tempOutsideFahrenheit != 999 )   
	{
		tempOutsideKelvin = devparam.tempOutsideCelsius;
		tempOutsideKelvin = tempOutsideKelvin + 273.15;
		return tempOutsideKelvin;		
	}
	
	if (devparam.tempOutsideFahrenheit != 999 & devparam.tempOutsideCelsius == 999)
	{
		
		tempOutsideKelvin = (devparam.tempOutsideFahrenheit - 32);
		tempOutsideKelvin = tempOutsideKelvin * 5;
		tempOutsideKelvin = tempOutsideKelvin / 9;
		tempOutsideKelvin = tempOutsideKelvin + 273.15;
		return tempOutsideKelvin;	
	}
	
	if (devparam.tempOutsideFahrenheit == 999 & devparam.tempOutsideCelsius == 999)
	{
		tempOutsideKelvin = 273.15 + 15;
		return tempOutsideKelvin; 
	}
	
	tempOutsideKelvin = 273.15 + 15;
	return tempOutsideKelvin;
}
float BME680_readPressure(void)
{
      if (devparam.pressOversampling == 0x00)						//disabling the pressure measurement function
      {
              return 0;
      }
      
      else
      {		
              BME680_readTempC();		
              
              uint32_t adc_P;
              adc_P = (uint32_t)I2C0ReadTransaction( BME680_I2C_ADDR,BME680_PRESSURE_MSB) << 12;
              adc_P |= (uint32_t)I2C0ReadTransaction( BME680_I2C_ADDR,BME680_PRESSURE_LSB) << 4;
              adc_P |= (I2C0ReadTransaction( BME680_I2C_ADDR,BME680_PRESSURE_XLSB) >> 4) & 0x0F;		
              
              int32_t var1 = 0;
              int32_t var2 = 0;
              int32_t var3 = 0;
              //int32_t var4 = 0;
              int32_t P = 0;
              
              var1 = (((int32_t)t_fine) >> 1) - 64000;
              
              var2 = ((((var1 >> 2) * (var1 >> 2)) >> 11) * (int32_t)bme680_coefficients.dig_P6) >> 2;
              var2 = var2 + ((var1 * (int32_t)bme680_coefficients.dig_P5) << 1);
              var2 = (var2 >> 2) + ((int32_t)bme680_coefficients.dig_P4 << 16);
              
              var1 = (((((var1 >> 2) * (var1 >> 2)) >> 13) * ((int32_t)bme680_coefficients.dig_P3 << 5)) >> 3) + (((int32_t)bme680_coefficients.dig_P2 * var1) >> 1);
              var1 = var1 >> 18;
              var1 = ((32768 + var1) * (int32_t)bme680_coefficients.dig_P1) >> 15;
              
              P = 1048576 - adc_P;
              P = (int32_t)((P - (var2 >> 12)) * ((uint32_t)3125));		
	
//This is the original calculation taken from the file "bme680_calculations.c"
//I chose to change part of the calculation, because the values I was getting were way off
	
              /*
              var4 = (1 << 31);
              if (P >= var4)
                      P = ((P / (uint32_t)var1) << 1);				
              else
                      P = ((P << 1) / (uint32_t)var1);
              */
		
//See what I did here? This corresponds to the "else" situation

              P = ((P << 1) / (uint32_t)var1);		
              
              var1 = ((int32_t)bme680_coefficients.dig_P9 * (int32_t)(((P >> 3) * (P >> 3)) >> 13)) >> 12;		
              var2 = ((int32_t)(P >> 2) * (int32_t)bme680_coefficients.dig_P8) >> 13;		
              var3 = ((int32_t)(P >> 8) * (int32_t)(P >> 8) * (int32_t)(P >> 8) * (int32_t)bme680_coefficients.dig_P10) >> 17;		
              
              P = (int32_t)(P) + ((var1 + var2 + var3 + ((int32_t)bme680_coefficients.dig_P7 << 7)) >> 4);
              
              return (float)P/100;		
      }
}
float BME680_readHumidity(void)
{
	if ( devparam.humidOversampling == 0x00)					//disabling the humitidy measurement function
	{
		return 0;
	}
	
	else
	{
		int32_t adc_H;
		adc_H = (uint32_t)I2C0ReadTransaction( BME680_I2C_ADDR,BME680_HUMIDITY_MSB) << 8;
		adc_H |= (uint32_t)I2C0ReadTransaction( BME680_I2C_ADDR,BME680_HUMIDITY_LSB);		
		
		int32_t temp_scaled = 0;
		int32_t var1 = 0;
		int32_t var2 = 0;
		int32_t var3 = 0;
		int32_t var4 = 0;
		int32_t var5 = 0;
		int32_t var6 = 0;
		int32_t H    = 0;
		
		temp_scaled = ((((int32_t)t_fine) * 5) + 128) >> 8;		
	
		var1 = (int32_t)adc_H - ((int32_t)((int32_t)bme680_coefficients.dig_H1 << 4)) - 
			   ((temp_scaled * (int32_t)bme680_coefficients.dig_H3/(int32_t)100) >> 1);
			   
		
		var2 = ((int32_t)bme680_coefficients.dig_H2 * (((temp_scaled * (int32_t)bme680_coefficients.dig_H4)/(int32_t)100) + 
				(((temp_scaled * ((temp_scaled * (int32_t)bme680_coefficients.dig_H5)/((int32_t)100))) >> 6)/((int32_t)100)) + 
				(int32_t)(1 << 14))) >> 10;
				
		var3 = var1 * var2;
		
		var4 = ((((int32_t)bme680_coefficients.dig_H6) << 7) + ((temp_scaled * (int32_t)bme680_coefficients.dig_H7)/((int32_t)100))) >> 4;
		
		var5 = ((var3 >> 14) * (var3 >> 14)) >> 10;
		var6 = (var4 * var5) >> 1;
		
		H = (var3 + var6) >> 12;
		
//This part avoids returning humidity values above 100% and below 0%, which wouldn't make sense		
		if (H > 102400)
			H = 102400;
		else if (H < 0)
			H = 0;
		
		return H/1024.;		
	}
}
float BME680_readAltitudeMeter(void)
{
	float heightOutput = 0;
	float tempOutsideKelvin = BME680_convertTempKelvin();
	
	heightOutput = BME680_readPressure();
	heightOutput = (heightOutput/devparam.pressureSeaLevel);
	heightOutput = pow(heightOutput, 0.190284);
	heightOutput = 1 - heightOutput;	
	heightOutput = heightOutput * tempOutsideKelvin;
	heightOutput = heightOutput / 0.0065;
	return heightOutput;		
}
//##########################################################################
//This is the same formula from before, but it converts the altitude from meters to feet before returning the results


float BME680_readAltitudeFeet(void)
{	
	float heightOutput = 0;
	float tempOutsideKelvin = BME680_convertTempKelvin();
	
	heightOutput = BME680_readPressure();
	heightOutput = (heightOutput/devparam.pressureSeaLevel);
	heightOutput = pow(heightOutput, 0.190284);
	heightOutput = 1 - heightOutput;
	heightOutput = heightOutput * tempOutsideKelvin;
	heightOutput = heightOutput / 0.0065;
	heightOutput = heightOutput / 0.3048;
	return heightOutput;	
}
float BME680_readGas(void)
//This is the last and final step
//Here we read the resistance of the gas sensor
//This is done on the loop function

{
	
	for (int i = 0; i <= 10; i++)
	{
		if (BME680_readStatus() == 1)
		{
			int16_t gas_r;	
			gas_r = (uint16_t)I2C0ReadTransaction( BME680_I2C_ADDR ,BME680_GAS_MSB) << 2;
			gas_r |= (uint16_t)((uint16_t)I2C0ReadTransaction( BME680_I2C_ADDR,BME680_GAS_LSB) >> 6);	
	
			int8_t gas_range;
			gas_range = (uint8_t)I2C0ReadTransaction( BME680_I2C_ADDR , BME680_GAS_LSB) & 0x0F;
	
			double var1;
			float gas_res;
	
			var1 = (1340.0 + 5.0 * bme680_coefficients.range_switching_error) * const_array1[gas_range];
			gas_res = var1 * const_array2[gas_range] / (gas_r - 512.0 + var1);	

			return gas_res;
		}
		
		osDelay(20);
	}
	
	return 0;	
	
}
uint8_t BME680_readStatus(void)
{
  uint8_t newDataBit = ((I2C0ReadTransaction( BME680_I2C_ADDR,0x1D) & 0x80) >> 7);      
	
  if (newDataBit == 1) 
  {                                                  
    uint8_t gasValidBit = ((I2C0ReadTransaction( BME680_I2C_ADDR ,0x2B) & 0x20) >> 5);
    uint8_t heaterStabilityBit = ((I2C0ReadTransaction( BME680_I2C_ADDR ,0x2B) & 0x10) >> 4);

    if ((gasValidBit == 1) & (heaterStabilityBit == 1))
    {
		
		return 1;
    }

    else
    {
      return 0;
    }
    
  }

  else
  {
    return 0;
  }

}
void BME680_writeCTRLMeas(void){
  uint8_t setvalue=0;
  setvalue = devparam.humidOversampling & 0x07;
  I2C0WriteTransaction( BME680_I2C_ADDR, BME680_CTRL_HUM, setvalue);
  
  setvalue = (devparam.tempOversampling << 5) & 0xE0;
  setvalue |= (devparam.pressOversampling << 2) & 0x1C;
  setvalue |= devparam.sensorMode & 0x03;
  I2C0WriteTransaction( BME680_I2C_ADDR, BME680_CTRL_MEAS, setvalue);
}
