/*
* bme680.h
*/
#include "MKL26Z4.h"
#include "EmbeddedTypes.h"

/***** DEFINE *****/
#define BME680_I2C_ADDR 0x76
#define BME680_CHIP_ID				0xD0
#define BME680_CTRL_HUM				0x72
#define BME680_CTRL_MEAS			0x74
#define BME680_CONFIG				0x75
#define BME680_TEMPERATURE_MSB		0x22
#define BME680_TEMPERATURE_LSB		0x23
#define BME680_TEMPERATURE_XLSB		0x24
#define BME680_PRESSURE_MSB			0x1F
#define BME680_PRESSURE_LSB			0x20
#define BME680_PRESSURE_XLSB		0x21
#define BME680_HUMIDITY_MSB			0x25
#define BME680_HUMIDITY_LSB			0x26
#define BME680_GAS_MSB				0x2A
#define BME680_GAS_LSB				0x2B
#define BME680_RESET    0xE0

#define	BME680_BIT_MASK_H1_DATA		0x0F

  /***** Coefficients *****/
#define	BME680_DIG_T1_LSB	0xE9
#define	BME680_DIG_T1_MSB	0xEA
#define	BME680_DIG_T2_LSB	0x8A
#define	BME680_DIG_T2_MSB	0x8B
#define	BME680_DIG_T3		0x8C
	
#define	BME680_DIG_P1_LSB	0x8E
#define	BME680_DIG_P1_MSB	0x8F
#define	BME680_DIG_P2_LSB	0x90
#define	BME680_DIG_P2_MSB	0x91
#define	BME680_DIG_P3		0x92
#define	BME680_DIG_P4_LSB	0x94
#define	BME680_DIG_P4_MSB	0x95
#define	BME680_DIG_P5_LSB	0x96
#define	BME680_DIG_P5_MSB	0x97
#define	BME680_DIG_P6		0x99
#define	BME680_DIG_P7		0x98
#define	BME680_DIG_P8_LSB	0x9C
#define	BME680_DIG_P8_MSB	0x9D
#define	BME680_DIG_P9_LSB	0x9E
#define	BME680_DIG_P9_MSB	0x9F
#define	BME680_DIG_P10		0xA0
	
#define	BME680_DIG_H1_LSB	0xE2
#define	BME680_DIG_H1_MSB	0xE3
#define	BME680_DIG_H2_LSB	0xE2
#define	BME680_DIG_H2_MSB	0xE1
#define	BME680_DIG_H3		0xE4
#define	BME680_DIG_H4		0xE5
#define	BME680_DIG_H5		0xE6
#define	BME680_DIG_H6		0xE7
#define	BME680_DIG_H7		0xE8
	
#define	BME680_DIG_G1		0xED
#define	BME680_DIG_G2_LSB	0xEB
#define	BME680_DIG_G2_MSB	0xEC
#define	BME680_DIG_G3		0xEE
	
#define	BME680_RES_HEAT_RG	0x02
#define	BME680_RES_HEAT_VL	0x00
	
#define	BME680_RES_HEAT_0	0x5A
#define	BME680_GAS_WAIT_0	0x64
	
#define	BME680_CTRL_GAS_1	0x71


struct BME680_Coefficients
{
    uint16_t dig_T1;
    int16_t  dig_T2;
    int8_t  dig_T3;

    uint16_t dig_P1;
    int16_t  dig_P2;
    int8_t   dig_P3;
    int16_t  dig_P4;
    int16_t  dig_P5;
    int8_t   dig_P6;
    int8_t   dig_P7;
    int16_t  dig_P8;
    int16_t  dig_P9;
    uint8_t  dig_P10;
    
    uint16_t dig_H1;
    uint16_t dig_H2;
    int8_t   dig_H3;
    int8_t   dig_H4;
    int8_t   dig_H5;
    uint8_t  dig_H6;
    int8_t   dig_H7;
    
    int8_t     par_g1;
    int16_t  par_g2;
    int8_t   par_g3;
    
    int8_t      res_heat_range;
    int8_t	res_heat_val;
    
    uint8_t	res_heat_0;	
    uint8_t	gas_wait_0;
    
    int8_t	gas_range;
    int8_t	range_switching_error;
	
}; 
static struct BME680_Coefficients bme680_coefficients;

struct DeviceParameter
{
	uint8_t communication;
	uint8_t I2CAddress;
	uint8_t sensorMode;
	uint8_t IIRfilter;
	uint8_t tempOversampling;
	uint8_t pressOversampling;
	uint8_t humidOversampling;
	uint16_t pressureSeaLevel;
	int16_t tempOutsideCelsius;
	int16_t tempOutsideFahrenheit;
	int16_t target_temp;
	int16_t amb_temp;
	uint8_t hotplate_profile;

};
static struct DeviceParameter devparam;

static int32_t t_fine;

// PROTOTYPE
uint8_t BME680_init(void);
void BME680_reset(void);
void BME680_setOversampling(void);
void BME680_setHeater(void);
void BME680_setFillter(void);
void BME680_readCoefficients(void);
void BME680_calculateHotPlateTime(void);
void BME680_calculateHotPlateRes(void);
void BME680_setHotPlateProfile(void);
uint8_t BME680_checkID(void);
float BME680_readTempC(void);
float BME680_readTempF(void);
float BME680_convertTempKelvin(void);
float BME680_readPressure(void);
float BME680_readHumidity(void);
float BME680_readAltitudeMeter(void);
float BME680_readAltitudeFeet(void);
float BME680_readGas(void);
uint8_t BME680_readStatus(void);
void BME680_writeCTRLMeas(void);
