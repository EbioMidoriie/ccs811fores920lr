//INCLUDE
#include "bme280.h"
#include "math.h"

void Pause_bme280( uint32_t time )
{
    uint32_t n;
    for( n = 1; n < time; n++ )
    {
        asm("nop");
    }
}


uint8_t bme280_readChipId(void){
    uint8_t ret = I2C0ReadTransaction( BME280_ADDRESS, BME280_CHIP_ID);
    return ret;
}

void bme280_softreset(void){
    I2C0WriteTransaction( BME280_ADDRESS, BME280_RESET, BME280_RESET_DATA );
}

void bme280_init(void){
    // SOFT RESET
    bme280_softreset();
    
    // CHECK CHIP ID
    while(1){
        Pause_bme280(300);
        
        if(bme280_readChipId() == 0x60){
          break;
        }
    }
    
    // SET CONFIG RESISTER
    uint8_t value = (BME280_STANDBY_TIME_10MS | BME280_FILTER_COEFFIENT_OFF | BME280_SPI_DISABLE );
    I2C0WriteTransaction( BME280_ADDRESS, BME280_CONFIG, value );
    
    // SET CTRL_MEAS RESISTER
    value = (BME280_TEMP_OVERSAMPLING_SKIP | BME280_PRESS_OVERSAMPLING_SKIP | BME280_MODE_NORMAL );
    I2C0WriteTransaction( BME280_ADDRESS, BME280_CTRL_MEAS, value );

    // SET CTRL_HUM RESISTER
    value = (BME280_HUMI_OVERSAMPLING_SKIP );
    I2C0WriteTransaction( BME280_ADDRESS, BME280_CTRL_HUM, value );
}

double bme280_readTemperature(void){
    double ret=0;
    uint8_t value=0;
    ret += ( I2C0ReadTransaction( BME280_ADDRESS, BME280_TEMP_MLB ) << 12 );
    ret += ( I2C0ReadTransaction( BME280_ADDRESS, BME280_TEMP_LSB ) << 4 );
    ret += ( I2C0ReadTransaction( BME280_ADDRESS, BME280_TEMP_XLSB ) >> 4 );
    
    return ret;
}

