/*
* fdc2214.h
*/
#include "MKL26Z4.h"
#include "EmbeddedTypes.h"

/***** DEFINE *****/
#define ADDR_SENSOR	0x2A
#define ADDR_SHT31	0x44

#define REG_CLOCK_DIVIDERS_CH0	0x14
#define REG_DRIVE_CURRENT_CH0	0x1E
#define REG_SETTLECOUNT_CH0	0x10
#define REG_RCOUNT_CH0	0x08
#define REG_ERROR_CONFIG	0x19
#define REG_MUX_CONFIG	0x1B
#define REG_CONFIG	0x1A

#define COM_SHT31_MSB	0x23
#define COM_SHT31_LSB	0x34

#define REG_CH0MSB	0x00
#define REG_CH0LSB	0x01

/***** PUBLIC FUNCTION PROTOTYPE *****/
void FDC2214_Init(void);
void FDC2214_Init_2ch(void);
uint8_t* FDC2214_Getdata( void );
uint8_t* FDC2214_Getdata_2ch( void );



