// INCLUDE
#include "i2c.h"


// DEFINE
#define HDC1080_ADDRESS 0x40

#define HDC1080_TEMPERATURE 0x00
#define HDC1080_HUMIDITY 0x01
#define HDC1080_CONFIGURATION 0x02
#define HDC1080_SERIAL_ID_FIRST 0xFB
#define HDC1080_SERIAL_ID_MIDDLE 0xFC
#define HDC1080_SERIAL_ID_LAST 0xFD
#define HDC1080_MANUFACTURAR_ID 0xFE
#define HDC1080_DEVICE_ID 0xFF

#define SOFT_RESET 0x80
#define HEATER 0x20
#define MODE_OF_ACQUISION 0x10
#define BATTERY_STATUS 0x08
#define TEMPERATURE_MEASUREMENT_RESOLUTION_14bit 0x00
#define TEMPERATURE_MEASUREMENT_RESOLUTION_11bit 0x04
#define HUMIDITY_MEASUREMENT_RESOLUTION_14bit 0x00
#define HUMIDITY_MEASUREMENT_RESOLUTION_11bit 0x01
#define HUMIDITY_MEASUREMENT_RESOLUTION_8bit 0x02

// PROTOTYPE
//    PUBLIC
extern int8_t hdc1080_Init(void);
extern uint16_t hdc1080_readConfiguration(void);
extern double hdc1080_readTemp(void);
extern double hdc1080_readHumi(void);
extern void hdc1080_I2C0readTransaction( unsigned char SlaveAddr, unsigned char u8Command, unsigned char bytes, unsigned char buf[] );

//    PRIVATE
void Pause_hdc1080( uint32_t time );
void hdc1080_softreset(void);
uint16_t hdc1080_getManufacturarID(void);
uint16_t hdc1080_getDeviceID(void);
