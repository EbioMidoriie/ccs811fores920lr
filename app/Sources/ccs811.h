#include "MKL26Z4.h"
#include "EmbeddedTypes.h"


//Register addresses
#define CSS811_STATUS		0x00
#define CSS811_MEAS_MODE	0x01
#define CSS811_ALG_RESULT_DATA 0x02
#define CSS811_RAW_DATA		0x03
#define CSS811_ENV_DATA		0x05
#define CSS811_NTC			0x06
#define CSS811_THRESHOLDS	0x10
#define CSS811_BASELINE		0x11
#define CSS811_HW_ID		0x20
#define CSS811_HW_VERSION	0x21
#define CSS811_FW_BOOT_VERSION 0x23
#define CSS811_FW_APP_VERSION 0x24
#define CSS811_ERROR_ID		0xE0
#define CSS811_APP_START	0xF4
#define CSS811_SW_RESET		0xFF

#define CCS811_I2C_ADDR		0x5A	// pin-ADDR is low->0x5A , pin-ADDR is Hi -> 0x5B

#define SUCCESS		0x00
#define HW_ID_ERROR	0x01
#define SENSOR_INTERNAL_ERROR	0x02
#define SENSOR_VALID_APP	0x03

#define CCS811_NTC = 0x06

#define CCS811_REF_RESISTOR			100000

// SET VALUE
#define DRIVE_MODE_0 (0x00<<4)
#define DRIVE_MODE_1 (0x01<<4)
#define DRIVE_MODE_2 (0x02<<4)
#define DRIVE_MODE_3 (0x03<<4)
#define DRIVE_MODE_4 (0x04<<4)

#define INT_DATARDY_INTERRUPT_DISABLE (0x00<<3)
#define INT_DATARDY_INTERRUPT_ENABLE (0x01<<3)

#define INT_THRESH_INTERRUPT_MODE_NORMALLY (0x00<<2)
#define INT_THRESH_INTERRUPT_MODE_ONLY_ASSERTS_nINT (0x01<<2)

// PROTOTYPE
extern uint8_t CCS811_Init(void);
extern uint8_t CCS811_beginCore(void);
uint8_t CCS811_readStatus(void);
extern void CCS811_setDriveMode(uint8_t mode);
extern uint8_t CCS811_dataAvailable(void);
extern uint8_t* CSS811_readAlgorithmResults( void );
int8_t Conv_bin2hex(uint8_t bins);
//double CSS811_calculateTemperature(void);
//void CCS811_readNTC(uint8_t reg, uint8_t *buf, uint8_t num);
extern void CSS811_setEnv(float tempC, float humi);
uint16_t ConvTempForReg(float temp);
uint16_t ConvHumiForReg(float humi);
void CCS811_ADC_START(void);