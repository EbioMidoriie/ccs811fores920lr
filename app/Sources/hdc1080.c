
// INCLUDE
#include "hdc1080.h"
#include "math.h"

void Pause_hdc1080( uint32_t time )
{
    uint32_t n;
    for( n = 1; n < time; n++ )
    {
        asm("nop");
    }
}

int8_t hdc1080_Init(void){
    // SOFT RESET
    hdc1080_softreset();
  
    // get Manufacturer ID
    uint16_t ManuID = hdc1080_getManufacturarID();
    if(ManuID!=0x5449){
        return -1;
    }
  
    // get Device ID
    uint16_t DevID = hdc1080_getDeviceID();
    if(DevID!=0x1050){
        return -2;
    }
    
    // set Configuration
    // ( Heater=ON, Mode=Temp first, TempResolution=11bit, HumiResolution=8bit )
    uint8_t set_value[2]={0};
    //set_value[0] = (HEATER | MODE_OF_ACQUISION | TEMPERATURE_MEASUREMENT_RESOLUTION_11bit | HUMIDITY_MEASUREMENT_RESOLUTION_8bit);
    set_value[0] = (MODE_OF_ACQUISION | TEMPERATURE_MEASUREMENT_RESOLUTION_14bit | HUMIDITY_MEASUREMENT_RESOLUTION_14bit);
    I2C0WriteMultiTransaction(HDC1080_ADDRESS, HDC1080_CONFIGURATION, set_value, 2);
    return 0;
}

void hdc1080_softreset(void){
    uint8_t value[2]={0};
    I2C0ReadMultiTransaction(HDC1080_ADDRESS, HDC1080_CONFIGURATION, 2, value);
    value[0] |= SOFT_RESET;
    I2C0WriteMultiTransaction(HDC1080_ADDRESS, HDC1080_CONFIGURATION, value, 2);
}

uint16_t hdc1080_getManufacturarID(void){
    uint8_t value[2]={0};
    I2C0ReadMultiTransaction(HDC1080_ADDRESS, HDC1080_MANUFACTURAR_ID, 2, value);
    uint16_t ret = (value[0]<<8 | value[1]);
    return ret;
}

uint16_t hdc1080_getDeviceID(void){
    uint8_t value[2]={0};
    I2C0ReadMultiTransaction(HDC1080_ADDRESS, HDC1080_DEVICE_ID, 2, value);
    uint16_t ret = (value[0]<<8 | value[1]);
    return ret;
}


uint16_t hdc1080_readConfiguration(void){
    uint8_t value[2] = {0};
    I2C0ReadMultiTransaction(HDC1080_ADDRESS, HDC1080_CONFIGURATION, 2, value);
    uint16_t ret = (value[0]<<8 | value[1]);
    return ret;
}

double hdc1080_readTemp(void){
  uint8_t value[4]={0};
  hdc1080_I2C0readTransaction(HDC1080_ADDRESS, HDC1080_TEMPERATURE, 4, value);
  uint16_t rawT = (value[0]<<8 | value[1]);
  return (rawT / pow(2, 16)) * 165.0 - 40.0;
}

double hdc1080_readHumi(void){
  uint8_t value[4]={0};
  hdc1080_I2C0readTransaction(HDC1080_ADDRESS, HDC1080_HUMIDITY, 4, value);
  uint16_t rawH = (value[0]<<8 | value[1]);
  return (rawH / pow(2, 16)) * 100.0;
}

void hdc1080_I2C0readTransaction( unsigned char SlaveAddr, unsigned char u8Command, unsigned char bytes, unsigned char buf[] )
{
    int i=0;
    unsigned char result;
    
    /* send data to slave */
    IIC0_StartTransmission( SlaveAddr, MWSR );
    i2c0_Wait();

    /* Write Command */
    I2C0_D = u8Command;
    i2c0_Wait();

    i2c0_Stop();

    Pause_hdc1080(1000);
    
    /* Send Slave Address */
    IIC0_StartTransmission( SlaveAddr, MWSR );
    i2c0_Wait();
    
    /* Put in Rx Mode */
    I2C0_C1 &= (~I2C_C1_TX_MASK);

    /* Ensure TXAK bit is 0 */
    I2C0_C1 &= ~I2C_C1_TXAK_MASK;

    /* Dummy read */
    result = I2C0_D;
    i2c0_Wait();

    for( i = 0; i < bytes - 2; i++ )
    {
        /* Read first byte */
        buf[i] = I2C0_D;
        i2c0_Wait();
    }

    /* Turn off ACK since this is second to last read*/
    I2C0_C1 |= I2C_C1_TXAK_MASK;

    /* Read second byte */
    buf[i++] = I2C0_D;
    i2c0_Wait();

    /* Send stop */
    i2c0_Stop();

    /* Read third byte */
    buf[i++] = I2C0_D;    
}
