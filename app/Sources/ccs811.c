/*
*	ccs811.c
*/

#include "i2c.h"
#include "ccs811.h"
#include "math.h"

/************************************************************************************
*
* Pause Routine
*
************************************************************************************/
void Pause2( uint32_t time )
{
    uint32_t n;
    for( n = 1; n < time; n++ )
    {
        asm("nop");
    }
}

uint8_t CCS811_Init(void){
	uint8_t data[4] = {0x11,0xE5,0x72,0x8A}; //Reset key
        uint8_t rcv_data[4]={0};

	//restart the core
	uint8_t returnError = CCS811_beginCore();
	if( returnError != SUCCESS ) return HW_ID_ERROR;
	//Reset the device
	I2C0WriteMultiTransaction( CCS811_I2C_ADDR, CSS811_SW_RESET, data, 4 );
	
        // spin
        Pause2(1000);
	

        // read bootloader register
	uint8_t HW_VERSION = I2C0ReadTransaction( CCS811_I2C_ADDR, 0x21);

	uint16_t FW_BOOT_VERSION = 0x0;
        I2C0ReadMultiTransaction( CCS811_I2C_ADDR, 0x23, 2, rcv_data );
        FW_BOOT_VERSION = (rcv_data[0]<<8 | rcv_data[1]<<8);
    
        uint16_t FW_APP_VERSION = 0x0;
        I2C0ReadMultiTransaction( CCS811_I2C_ADDR, 0x24, 2, rcv_data );
        FW_APP_VERSION = (rcv_data[0]<<8 | rcv_data[1]<<8);

        uint8_t ERROR_IDs = I2C0ReadTransaction( CCS811_I2C_ADDR, 0xE0 );
        
        
	uint8_t s_status = CCS811_readStatus();
        if( (s_status&0x01) != 0x00 ) return SENSOR_INTERNAL_ERROR;
	if( ( (s_status>>4)&0x01 ) != 0x01 ) return SENSOR_VALID_APP;
	
        CCS811_ADC_START();
        
	s_status = CCS811_readStatus();
	if( ( (s_status>>7)&0x01 ) != 0x01 ) return SENSOR_VALID_APP;
        
	CCS811_setDriveMode(1); // measure every second
	return SUCCESS;
}

void CCS811_ADC_START(void){
	I2C0WriteTransactionOnly( CCS811_I2C_ADDR, CSS811_APP_START, 0x01 );
}

uint8_t CCS811_beginCore(void){
	//Spin for a few ms
        // spin
        Pause2(1000);
	
	//Check the ID register to determine if the operation was a success.
        uint8_t returnValue = SUCCESS;
	uint8_t readCheck = I2C0ReadTransaction( CCS811_I2C_ADDR, CSS811_HW_ID);
	if( readCheck != 0x81 )
	{
		returnValue = HW_ID_ERROR;
	}

	return returnValue;
        Pause2(1000);
}

uint8_t CCS811_readStatus(void){
	uint8_t result = I2C0ReadTransaction( CCS811_I2C_ADDR, CSS811_STATUS);
	return result;
}


void CCS811_setDriveMode(uint8_t mode){
  // Address:0x01 bit4-6
  // 000 : no measurement
  // 001 : measurement every second
  // 010 : measurement every 10 seconds
  // 011 : measurement every 60 seconds
  // 100 : measurement every 250 mill seconds
  
	uint8_t value = (DRIVE_MODE_1 | INT_DATARDY_INTERRUPT_DISABLE | INT_THRESH_INTERRUPT_MODE_NORMALLY); //Mask in mode
	I2C0WriteTransaction( CCS811_I2C_ADDR, CSS811_MEAS_MODE, value);
}

uint8_t CCS811_dataAvailable(void){
        uint8_t ret = 0;
        
	uint8_t STATUS_RESULT = I2C0ReadTransaction( CCS811_I2C_ADDR, CSS811_STATUS);
	uint8_t ERROR_RESULT = I2C0ReadTransaction( CCS811_I2C_ADDR, CSS811_ERROR_ID);
        
        if( ((STATUS_RESULT>>3)&0x01) == 1 )    ret=0x01;
        
	return ret;
}

uint8_t* CSS811_readAlgorithmResults( void ){
	uint8_t data_eCO2[8] = {0};
        /*
	uint8_t data_TVOC[4] = {0};
	uint8_t data_STS_ERR[2] = {0};
	uint8_t data_RAW[2] = {0};
        */
        //I2C0ReadMultiTransactionNAK( CCS811_I2C_ADDR, CSS811_ALG_RESULT_DATA, 8, data );
        //I2C0ReadMultiTransaction3( CCS811_I2C_ADDR, CSS811_ALG_RESULT_DATA, 8, data );
        I2C0ReadMultiTransaction( CCS811_I2C_ADDR, CSS811_ALG_RESULT_DATA, 8, data_eCO2 );
        //I2C0ReadMultiTransaction4( CCS811_I2C_ADDR, CSS811_ALG_RESULT_DATA, 4, data_TVOC );
        //I2C0ReadMultiTransaction4( CCS811_I2C_ADDR, CSS811_ALG_RESULT_DATA, 2, data_STS_ERR );
        //I2C0ReadMultiTransaction4( CCS811_I2C_ADDR, CSS811_ALG_RESULT_DATA, 2, data_RAW );
        //I2C0ReadMultiTransaction_NoREG( CCS811_I2C_ADDR, CSS811_ALG_RESULT_DATA, 2, data2 );
        //I2C0ReadMultiTransaction_NoREG( CCS811_I2C_ADDR, CSS811_ALG_RESULT_DATA, 2, data3 );

	// Data ordered:
	// co2MSB, co2LSB, <TAB>, tvocMSB, tvocLSB
	
	//CO2 = ((uint16_t)data[0] << 8) | data[1];
	//tVOC = ((uint16_t)data[2] << 8) | data[3];
	
	uint8_t Ascii[24]={0};
        
        
	// CO2 date
	Ascii[0] = (uint8_t)Conv_bin2hex((data_eCO2[0]&0xF0)>>4);
	Ascii[1] = (uint8_t)Conv_bin2hex((data_eCO2[0]&0x0F));
	Ascii[2] = (uint8_t)Conv_bin2hex((data_eCO2[1]&0xF0)>>4);
	Ascii[3] = (uint8_t)Conv_bin2hex((data_eCO2[1]&0x0F));
	Ascii[4] = (uint8_t)'\t';
	
	// tVOC data
	Ascii[5] = (uint8_t)Conv_bin2hex((data_eCO2[2]&0xF0)>>4);
	Ascii[6] = (uint8_t)Conv_bin2hex((data_eCO2[2]&0x0F));
	Ascii[7] = (uint8_t)Conv_bin2hex((data_eCO2[3]&0xF0)>>4);
	Ascii[8] = (uint8_t)Conv_bin2hex((data_eCO2[3]&0x0F));
	Ascii[9] = '\t';
        
        /*
        // STATUS
	Ascii[10] = (uint8_t)Conv_bin2hex((data_STS_ERR[4]&0xF0)>>4);
	Ascii[11] = (uint8_t)Conv_bin2hex((data_STS_ERR[4]&0x0F));
	Ascii[12] = '\t';
        
        // ERROR_ID
	Ascii[13] = (uint8_t)Conv_bin2hex((data_STS_ERR[5]&0xF0)>>4);
	Ascii[14] = (uint8_t)Conv_bin2hex((data_STS_ERR[5]&0x0F));
	Ascii[15] = '\t';
        
        // See RAW DATA
	Ascii[16] = (uint8_t)Conv_bin2hex((data_RAW[6]&0xF0)>>4);
	Ascii[17] = (uint8_t)Conv_bin2hex((data_RAW[6]&0x0F));
	Ascii[18] = (uint8_t)Conv_bin2hex((data_RAW[7]&0xF0)>>4);
	Ascii[19] = (uint8_t)Conv_bin2hex((data_RAW[7]&0x0F));
	Ascii[20] = '\0';
        */
        
	return Ascii;
}

/*
void CCS811_readNTC(uint8_t reg, uint8_t *buf, uint8_t num)
{
	uint8_t value;
	uint8_t pos = 0;
	
	//on arduino we need to read in 32 byte chunks
	while(pos < num){
		
		uint8_t read_now = min(32, num - pos);
              	I2C0WriteTransaction( CCS811_I2C_ADDR, reg , 0x01 );

                
		//Wire.beginTransmission((uint8_t)_i2caddr);
		//Wire.write((uint8_t)reg + pos);
		//Wire.endTransmission();
                
                
                I2C0ReadTransaction( CCS811_I2C_ADDR, );
                
		//Wire.requestFrom((uint8_t)_i2caddr, read_now);
		
		for(int i=0; i<read_now; i++){
			buf[pos] = Wire.read();
			pos++;
		}
	}
}

double CSS811_calculateTemperature(void){
	uint8_t buf[4];
	CSS811_readNTC(CCS811_NTC, buf, 4);

	uint32_t vref = ((uint32_t)buf[0] << 8) | buf[1];
	uint32_t vntc = ((uint32_t)buf[2] << 8) | buf[3];
	
	//from ams ccs811 app note
	uint32_t rntc = vntc * CCS811_REF_RESISTOR / vref;
	
	double ntc_temp;
	ntc_temp = log((double)rntc / CCS811_REF_RESISTOR); // 1
	ntc_temp /= 3380; // 2
	ntc_temp += 1.0 / (25 + 273.15); // 3
	ntc_temp = 1.0 / ntc_temp; // 4
	ntc_temp -= 273.15; // 5
	return ntc_temp - _tempOffset;
}
*/

void CSS811_setEnv(float tempC, float humi){
        
  uint16_t reg_humi = ConvHumiForReg(humi);
  uint16_t reg_temp = ConvHumiForReg(tempC);
  
  uint8_t data[4] = { (uint8_t)(reg_humi>>8)&0x0F,
                      (uint8_t)(reg_humi)&0x0F,
                      (uint8_t)(reg_temp>>8)&0x0F,
                      (uint8_t)(reg_humi)&0x0F }; //Reset key
  I2C0WriteMultiTransaction( CCS811_I2C_ADDR, CSS811_ENV_DATA, data, 4 );
}

int8_t Conv_bin2hex(uint8_t bins){
  uint8_t hd[] = "0123456789ABCDEF" ;
  return(hd[bins]);
}

uint16_t ConvTempForReg(float temp){
  uint16_t ret=0;
  ret = (uint16_t)(temp/1.00)<<9;

  float vals = temp - (int)temp%1;
  if(vals>=0.50){
      ret |= 0x10;
      vals = vals-0.5;
  }
  if(vals>=0.25){
    ret |= 0x08;
    vals = vals-0.25;
  }
  if(vals>=0.125){
    ret |= 0x04;
  }
  return ret;
}

uint16_t ConvHumiForReg(float humi){
  uint16_t ret=0;
  ret = (uint16_t)(humi/1.00)<<9;

  float vals = humi - (int)humi%1;
  if(vals>=0.50){
      ret |= 0x10;
      vals = vals-0.5;
  }
  if(vals>=0.25){
    ret |= 0x08;
    vals = vals-0.25;
  }
  if(vals>=0.125){
    ret |= 0x04;
  }
  return ret;
}