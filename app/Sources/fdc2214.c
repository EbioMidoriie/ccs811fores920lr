/*
* fdc2214.h
*/
#include "i2c.h"
#include "fdc2214.h"

/***** PRIVATE FUNCTION PROTPTYPE *****/
int8_t Conv_bin2hex(uint8_t bins);

/***** PUBLIC FUNCTION PROTOTYPE *****/

/***** FUNCTIONS *****/
static void FDC2214_Init( void )
{
  	unsigned char setvalue[2]={0,0};
	
	setvalue[0] = 0x20;
	setvalue[1] = 0x01;
	I2C0WriteMultiTransaction( ADDR_SENSOR, REG_CLOCK_DIVIDERS_CH0, setvalue , 0x2 );
        
	setvalue[0] = 0xF8;
	setvalue[1] = 0x00;
	I2C0WriteMultiTransaction( ADDR_SENSOR, REG_DRIVE_CURRENT_CH0, setvalue , 0x2 );

	setvalue[0] = 0x00;
	setvalue[1] = 0x0A;
	I2C0WriteMultiTransaction( ADDR_SENSOR, REG_SETTLECOUNT_CH0, setvalue , 0x2 );

	setvalue[0] = 0x69;
	setvalue[1] = 0xE8;
	I2C0WriteMultiTransaction( ADDR_SENSOR, REG_RCOUNT_CH0, setvalue , 0x2 );

	setvalue[0] = 0x00;
	setvalue[1] = 0x00;
	I2C0WriteMultiTransaction( ADDR_SENSOR, REG_ERROR_CONFIG, setvalue , 0x2 );

	setvalue[0] = 0x02;
	setvalue[1] = 0x0C;
	I2C0WriteMultiTransaction( ADDR_SENSOR, REG_MUX_CONFIG, setvalue , 0x2 );

	setvalue[0] = 0x14;
	setvalue[1] = 0x41;
	I2C0WriteMultiTransaction( ADDR_SENSOR, REG_CONFIG, setvalue , 0x2 );
}

static void FDC2214_Init_2ch( void )
{
  	unsigned char setvalue[2]={0,0};
	
        /* SETTING SETTLECOUNT */
        /* CH0 */
	setvalue[0] = 0x04;
	setvalue[1] = 0x00;
	I2C0WriteMultiTransaction( ADDR_SENSOR, 0x10, setvalue , 0x2 );

        /* CH1 */
	I2C0WriteMultiTransaction( ADDR_SENSOR, 0x11, setvalue , 0x2 );

        /* SETTING CLOCK_DIVIDERS */
        /* CH0 */
	setvalue[0] = 0x10;
	setvalue[1] = 0x01;
	I2C0WriteMultiTransaction( ADDR_SENSOR, 0x14, setvalue , 0x2 );

        /* CH1 */
	I2C0WriteMultiTransaction( ADDR_SENSOR, 0x15, setvalue , 0x2 );

        /* SETTING ERROR CONFIG */
	setvalue[0] = 0x00;
	setvalue[1] = 0x01;
	I2C0WriteMultiTransaction( ADDR_SENSOR, 0x19, setvalue , 0x2 );

        /* SETTING CONFIG */
	setvalue[0] = 0x1e;
	setvalue[1] = 0x01;
	I2C0WriteMultiTransaction( ADDR_SENSOR, 0x1A, setvalue , 0x2 );
        
        /* SETTING MUX_CONFIG */
	setvalue[0] = 0x82;
	setvalue[1] = 0x0C;
	I2C0WriteMultiTransaction( ADDR_SENSOR, 0x1B, setvalue , 0x2 );

        /* SETTING RESET_DEV */
	setvalue[0] = 0x00;
	setvalue[1] = 0x00;
	I2C0WriteMultiTransaction( ADDR_SENSOR, 0x1C, setvalue , 0x2 );
        
        /* SETTING DRIVE_CURRENT */
        /* CH0 */
	setvalue[0] = 0x8C;
	setvalue[1] = 0x40;
	I2C0WriteMultiTransaction( ADDR_SENSOR, 0x1E, setvalue , 0x2 );
        
        /* CH1 */
	I2C0WriteMultiTransaction( ADDR_SENSOR, 0x1F, setvalue , 0x2 );
        
}

unsigned char* FDC2214_Getdata( void )
{
    uint8_t     result;
    uint8_t     val_msb[2]={0,0};
    uint8_t     val_lsb[2]={0,0};
    uint8_t     sensor_result[4]={0,0,0,0};
 
    I2C0ReadMultiTransaction( ADDR_SENSOR, 0x00, 2, val_msb );
    I2C0ReadMultiTransaction( ADDR_SENSOR, 0x01, 2, val_lsb );

    //result = CheckCrc( val, 2, val[2] );
    uint16_t cap_CH0_MSB = (uint16_t)((val_msb[0] << 8) | val_msb[1]);
    uint16_t cap_CH0_LSB = (uint16_t)((val_lsb[0] << 8) | val_lsb[1]);
    unsigned long cap_CH0 = (unsigned long)(cap_CH0_MSB<<16 | cap_CH0_LSB);
    
    sensor_result[0] = val_msb[0];
    sensor_result[1] = val_msb[1];
    sensor_result[2] = val_lsb[0];
    sensor_result[3] = val_lsb[1];
    
    return( sensor_result );
}

uint8_t* FDC2214_Getdata_2ch( void )
{
    uint8_t     result;
    uint8_t     val_msb[2]={0,0};
    uint8_t     val_lsb[2]={0,0};
    uint8_t     sensor_result[4]={0,0,0,0};
    uint8_t Ascii[24]={0};
 
    // CH0
    I2C0ReadMultiTransaction( ADDR_SENSOR, 0x00, 2, val_msb );
    I2C0ReadMultiTransaction( ADDR_SENSOR, 0x01, 2, val_lsb );

    //result = CheckCrc( val, 2, val[2] );
    /*
    uint16_t cap_CH0_MSB = (uint16_t)((val_msb[0] << 8) | val_msb[1]);
    uint16_t cap_CH0_LSB = (uint16_t)((val_lsb[0] << 8) | val_lsb[1]);
    unsigned long cap_CH0 = (unsigned long)(cap_CH0_MSB<<16 | cap_CH0_LSB);
    */
    
    sensor_result[0] = val_msb[0];
    sensor_result[1] = val_msb[1];
    sensor_result[2] = val_lsb[0];
    sensor_result[3] = val_lsb[1];
    
    Ascii[0] = (uint8_t)Conv_bin2hex((val_msb[0]&0xF0)>>4);
    Ascii[1] = (uint8_t)Conv_bin2hex(val_msb[0]&0x0F);
    Ascii[2] = (uint8_t)Conv_bin2hex((val_msb[1]&0xF0)>>4);
    Ascii[3] = (uint8_t)Conv_bin2hex(val_msb[1]&0x0F);
    Ascii[4] = (uint8_t)Conv_bin2hex((val_lsb[0]&0xF0)>>4);
    Ascii[5] = (uint8_t)Conv_bin2hex(val_lsb[0]&0x0F);
    Ascii[6] = (uint8_t)Conv_bin2hex((val_lsb[1]&0xF0)>>4);
    Ascii[7] = (uint8_t)Conv_bin2hex(val_lsb[1]&0x0F);
    Ascii[8] = (uint8_t)'\t';

    // CH1
    I2C0ReadMultiTransaction( ADDR_SENSOR, 0x02, 2, val_msb );
    I2C0ReadMultiTransaction( ADDR_SENSOR, 0x03, 2, val_lsb );

    sensor_result[0] = val_msb[0];
    sensor_result[1] = val_msb[1];
    sensor_result[2] = val_lsb[0];
    sensor_result[3] = val_lsb[1];

    Ascii[9] = (uint8_t)Conv_bin2hex((val_msb[0]&0xF0)>>4);
    Ascii[10] = (uint8_t)Conv_bin2hex(val_msb[0]&0x0F);
    Ascii[11] = (uint8_t)Conv_bin2hex((val_msb[1]&0xF0)>>4);
    Ascii[12] = (uint8_t)Conv_bin2hex(val_msb[1]&0x0F);
    Ascii[13] = (uint8_t)Conv_bin2hex((val_lsb[0]&0xF0)>>4);
    Ascii[14] = (uint8_t)Conv_bin2hex(val_lsb[0]&0x0F);
    Ascii[15] = (uint8_t)Conv_bin2hex((val_lsb[1]&0xF0)>>4);
    Ascii[16] = (uint8_t)Conv_bin2hex(val_lsb[1]&0x0F);
    Ascii[17] = (uint8_t)'\0';

    
    return( Ascii );
}

int8_t Conv_bin2hex(uint8_t bins){
  uint8_t hd[] = "0123456789ABCDEF" ;
  return(hd[bins]);
}
